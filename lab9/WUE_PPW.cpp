﻿#include <iostream>
#include <string>
#include <stdio.h.>
#include <windows.h>
#include <stdlib.h>
#include <vector>
using namespace std;

struct Person
{
	string surname;
	string name;
	string patronymic;
	string address;

	union Number
	{
		char telephonNumber[20];
		char homeNumber[20];
	}phone;

	string age;
	bool delete_struct : 1;
	bool num_check;
};

enum menu { input = 1, output, sort, search, change, struct_delete, struct_exit };

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	const int arr_capacity = 3;
	Person* arr = new Person[arr_capacity];
	Person* p = arr;

	int choice = 0;

	for (int i = 0; i < arr_capacity; i++)
	{
		p[i].delete_struct = 1;
	}

	while (choice != 7)
	{

		cout << "\n-------------------------------------------------------------------------------------------------\n"
			"\n1) Ввод массива структур;\n"
			"2) Вывод массива структур;\n"
			"3) Сортировка массива структур;\n"
			"4) Поиск в массиве структур по заданному параметру;\n"
			"5) Изменение заданной структуры;\n"
			"6) Удаление структуры из массива;\n"
			"7) Выход.\n\n"
			"Выберите один из вариантов (1-7): ";

		cin >> choice;
		cout << "\n\n";
		switch (choice)
		{

		case input: //ввод массива структур
		{

			for (int i = 0; i < arr_capacity; i++)
			{

				cout << "Структура №" << i + 1 << "\n\n";

				cout << "Введите Фамилию: ";

				cin.ignore(256, '\n') >> p[i].surname;
				cout << "Введите Имя: ";

				cin.ignore(256, '\n') >> p[i].name;
				cout << "Введите Отчество: ";

				(cin >> p[i].patronymic).get();
				cout << "Введите Адрес: ";

				getline(cin, p[i].address);
				cout << "Мобильный (0) или домашний телефон (1): ";
				cin >> p[i].num_check;

				switch (p[i].num_check)
				{

				case 0:
				{
					cout << "Введите мобильный телефонный номер: ";
					cin >> p[i].phone.telephonNumber;
					break;
				}

				case 1:
				{
					cout << "Введите домашний телефонный номер: ";
					cin >> p[i].phone.homeNumber;
					break;
				}

				}

				cout << "Введите Возраст: ";

				cin.ignore(256, '\n') >> p[i].age;
				cout << "\n\n";

				p[i].delete_struct = 0;
			}

			break;
		}

		case output: //вывод массива структур
		{
			for (int i = 0; i < arr_capacity; i++)
			{
				if (p[i].delete_struct == 0)
				{
					cout << "\n\nСтруктура №" << i + 1;

					cout << "\n\nФамилия: " << p[i].surname
						<< "\nИмя: " << p[i].name
						<< "\nОтчество: " << p[i].patronymic
						<< "\nАдрес: " << p[i].address;

					switch (p[i].num_check)
					{
					case 0:
						cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
						break;

					case 1:
						cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
						break;
					}

					cout << "\nВозраст: " << p[i].age
						<< "\n";
				}
			}

			break;
		}

		case sort: //сортировка массива структур
		{
			Person Tmp;
			for (int i = 0; i < arr_capacity; i++)
			{
				for (int j = 0; j < arr_capacity; j++)
				{
					if (p[i].surname[0] < p[j].surname[0])
					{
						Tmp = p[i];
						p[i] = p[j];
						p[j] = Tmp;
					}
				}
			}
			cout << "\n\nОтсортировано\n\n";

			break;
		}

		case search: //поиск по массиву структур
		{
			int search = 0;
			string search_str;
			cout << "\n1.Фамилия\n2.Имя\n3.Отчество\n4.Адрес\n5.Телефонный номер\n6.Возраст\n\nВыберите параметр поиска(1-6) : ";

			cin >> search;

			cout << "\n";

			switch (search)
			{

			case 1:
			{
				cout << "Фамилия для поиска: ";
				cin >> search_str;
				cout << "\n";

				for (int i = 0; i < arr_capacity; i++)
				{
					if (search_str == p[i].surname && p[i].delete_struct == 0)
					{
						cout << "\n\nФамилия найдена в структуре №: " << i + 1 << "\n\n"

							<< "Фамилия: " << p[i].surname
							<< "\nИмя: " << p[i].name
							<< "\nОтчество: " << p[i].patronymic
							<< "\nАдрес: " << p[i].address;

						switch (p[i].num_check)
						{
						case 0:
							cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
							break;

						case 1:
							cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
							break;
						}

						cout << "\nВозраст: " << p[i].age
							<< "\n";
					}
				}
				break;
			}

			case 2:
			{
				cout << "Имя для поиска: ";
				cin >> search_str;
				cout << "\n";

				for (int i = 0; i < arr_capacity; i++)
				{
					if (search_str == p[i].name && p[i].delete_struct == 0)
					{
						cout << "\n\nИмя найдено в структуре №: " << i + 1 << "\n\n"

							<< "Фамилия: " << p[i].surname
							<< "\nИмя: " << p[i].name
							<< "\nОтчество: " << p[i].patronymic
							<< "\nАдрес: " << p[i].address;

						switch (p[i].num_check)
						{
						case 0:
							cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
							break;

						case 1:
							cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
							break;
						}

						cout << "\nВозраст: " << p[i].age
							<< "\n";
					}
				}
				break;
			}

			case 3:
			{
				cout << "Отчество для поиска: ";
				cin >> search_str;
				cout << "\n";

				for (int i = 0; i < arr_capacity; i++)
				{
					if (search_str == p[i].patronymic && p[i].delete_struct == 0)
					{
						cout << "\n\nОтчество найдено в структуре №: " << i + 1 << "\n\n"

							<< "Фамилия: " << p[i].surname
							<< "\nИмя: " << p[i].name
							<< "\nОтчество: " << p[i].patronymic
							<< "\nАдрес: " << p[i].address;

						switch (p[i].num_check)
						{
						case 0:
							cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
							break;

						case 1:
							cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
							break;
						}
						cout << "\nВозраст: " << p[i].age
							<< "\n";
					}
				}
				break;
			}

			case 4:
			{
				cout << "Адрес для поиска: ";
				cin >> search_str;
				cout << "\n";

				for (int i = 0; i < arr_capacity; i++)
				{
					if (search_str == p[i].address && p[i].delete_struct == 0)
					{
						cout << "\n\nАдрес найден в структуре №: " << i + 1 << "\n\n"

							<< "Фамилия: " << p[i].surname
							<< "\nИмя: " << p[i].name
							<< "\nОтчество: " << p[i].patronymic
							<< "\nАдрес: " << p[i].address;

						switch (p[i].num_check)
						{
						case 0:
							cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
							break;

						case 1:
							cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
							break;
						}

						cout << "\nВозраст: " << p[i].age
							<< "\n";
					}
				}
				break;
			}

			case 5:
			{
				cout << "Телефонный номер для поиска: ";
				cin >> search_str;
				cout << "\n";

				for (int i = 0; i < arr_capacity; i++)
				{
					if ((search_str == p[i].phone.telephonNumber || search_str == p[i].phone.homeNumber) && p[i].delete_struct == 0)
					{
						cout << "\n\nТелефонный номер найден в структуре №: " << i + 1 << "\n\n"

							<< "Фамилия: " << p[i].surname
							<< "\nИмя: " << p[i].name
							<< "\nОтчество: " << p[i].patronymic
							<< "\nАдрес: " << p[i].address;

						switch (p[i].num_check)
						{
						case 0:
							cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
							break;

						case 1:
							cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
							break;
						}

						cout << "\nВозраст: " << p[i].age
							<< "\n";
					}
				}
				break;
			}

			case 6:
			{
				cout << "Возраст для поиска: ";
				cin >> search_str;
				cout << "\n";

				for (int i = 0; i < arr_capacity; i++)
				{
					if (search_str == p[i].age && p[i].delete_struct == 0)
					{
						cout << "\n\nВозраст найден в структуре №: " << i + 1 << "\n\n"

							<< "Фамилия: " << p[i].surname
							<< "\nИмя: " << p[i].name
							<< "\nОтчество: " << p[i].patronymic
							<< "\nАдрес: " << p[i].address;

						switch (p[i].num_check)
						{
						case 0:
							cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
							break;

						case 1:
							cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
							break;
						}

						cout << "\nВозраст: " << p[i].age
							<< "\n";
					}
				}
				break;
			}

			default:
			{
				cout << "Некорректный ввод";
				break;
			}
			break;
			}
			break;
		}

		case change: //Изменение структуры
		{
			int change_struct = 0;

			for (int i = 0; i < arr_capacity; i++)
			{
				if (p[i].delete_struct == 0)
				{
					cout << "\n\nCтруктура №: " << i + 1 << "\n\n"

						<< "Фамилия: " << p[i].surname
						<< "\nИмя: " << p[i].name
						<< "\nОтчество: " << p[i].patronymic
						<< "\nАдрес: " << p[i].address;

					switch (p[i].num_check)
					{
					case 0:
						cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
						break;

					case 1:
						cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
						break;
					}

					cout << "\nВозраст: " << p[i].age
						<< "\n";
				}
			}

			cout << "\n\nВыберите номер структуры, которую нужно изменить: ";
			cin >> change_struct;
			string new_parameter;
			int ch = 0;
			while (ch != 7) 
			{
				cout << "\n\nВыбор поля для изменения (1-6)\nВыход в главное меню (7)\nВаш выбор:";
				cin >> ch;

				switch (ch)
				{
				case 1:
					cout << "\nНовая фамилия: ";
					cin >> p[change_struct - 1].surname;
					break;
				case 2:
					cout << "\nНовое имя: ";
					cin >> p[change_struct - 1].name;
					break;
				case 3:
					cout << "\nНовое отчество: ";
					cin >> p[change_struct - 1].patronymic;
					break;
				case 4:
					cout << "\nНовый адрес: ";
					cin >> p[change_struct - 1].address;
					break;
				case 5:
					switch (p[change_struct - 1].num_check)
					{

					case 0:
					{
						cout << "\nНовый мобильный телефонный номер: ";
						cin >> p[change_struct - 1].phone.telephonNumber;
						break;
					}

					case 1:
					{
						cout << "\nНовый домашний телефонный номер: ";
						cin >> p[change_struct - 1].phone.homeNumber;
						break;
					}
					break;
					}
				case 6:
					cout << "\nНовый возраст: ";
					cin >> p[change_struct - 1].age;
					break;

				case 7:
					break;
				}
			}
			break;
		}

		case struct_delete: //удаление структуры
		{
			for (int i = 0; i < arr_capacity; i++)
			{
				cout << "\n\nCтруктура №: " << i + 1 << "\n\n"
					<< "Фамилия: " << p[i].surname
					<< "\nИмя: " << p[i].name
					<< "\nОтчество: " << p[i].patronymic
					<< "\nАдрес: " << p[i].address;

				switch (p[i].num_check)
				{
				case 0:
					cout << "\nМобильный телефонный номер: " << p[i].phone.telephonNumber;
					break;

				case 1:
					cout << "\nДомашний телефонный номер: " << p[i].phone.homeNumber;
					break;
				}

				cout << "\nВозраст: " << p[i].age
					<< "\n\n";
			}

			cout << "Выберите номер структуры, которую нужно удалить: ";

			int delete_structure = 0;
			cin >> delete_structure;
			if (delete_structure > 0 && delete_structure < arr_capacity + 1)
			{
				delete_structure--;

				for (int i = delete_structure; i < arr_capacity; i++)
				{
					p[delete_structure].delete_struct = 1;
				}

				cout << "\nУдалено";
			}

			else {
				cout << "Некорректный ввод";
			}
			break;
		}

		case struct_exit:
		{
			cout << "Выход";
			break;
		}

		default:
		{
			cout << "Некорректный ввод";
			break;
		}

		}
	}
	delete[] p;
	return 0;
}
