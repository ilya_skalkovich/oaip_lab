﻿#include <iostream>
#include <ctime>
#include <iterator>
#include <clocale>
#include <windows.h>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
using namespace std;

template<typename T>
class List 
{
public:
	List() 
	{
		listSize = 0;
		head = nullptr;
	}
	~List() 
	{
		while (listSize)
		{
			Node<T>* temp = head;
			head = head->next;
			delete temp;
			listSize--;
		}
	}

	void erase(int index) 
	{
		if (index == 0)
		{
			Node<T>* temp = head;
			head = head->next;
			delete temp;
			listSize--;
		}

		else
		{
			Node<T>* previous = this->head;

			for (int i = 0; i < index - 1; i++) 
				previous = previous->next;
			
			Node<T>* Delete = previous->next;
			previous->next = Delete->next;

			delete Delete;
			listSize--;
		}
	}

	void insert(T data, int index) {
		if (index == 0) 
			push_front(data);
		
		else 
		{
			Node<T>* previous = this->head;

			for (int i = 0; i < index - 1; i++) 
				previous = previous->next;
			
			previous->next = new Node<T>(data, previous->next);
			listSize++;
		}
	}

	void push_front(T data) 
	{
		head = new Node<T>(data, head);
		listSize++;
	}

	void push_back(T data) 
	{
		if (head == nullptr) 
			head = new Node<T>(data);
	
		else 
		{
			Node<T>* current = this->head;

			while (current->next != nullptr) 
				current = current->next;
			
			current->next = new Node<T>(data);
		}

		listSize++;
	}

	int getListSize() 
	{ 
		return listSize;
	}

	T& operator[](const int index) 
	{
		int counter = 0;
		Node<T>* current = this->head;

		while (current != nullptr) 
		{
			if (counter == index) 
				return current->data;
			
			current = current->next;
			counter++;
		}
	}

private:
	template<typename T>
	class Node 
	{
	public:
		Node* next;
		T data;
		Node(T data = T(), Node* next = nullptr) 
		{
			this->data = data;
			this->next = next;
		}
	};

	int listSize;
	Node<T>* head;
};

struct TNode 
{
	int num;
	struct TNode *left, *right;

	TNode(int num, TNode* left = NULL, TNode* right = NULL)
	{
		this->num = num;
		this->left = left;
		this->right = right;
	}
};


struct TNode* newTNode(int num, TNode* tree) 
{
	if (tree == NULL) tree = new TNode(num);
	else  if (num > tree->num) tree->right = newTNode(num, tree->right);
	else tree->left = newTNode(num, tree->left);

	return(tree);
}


void PrintTree(TNode* TNode) 
{
	if (TNode != NULL)
	{
		PrintTree(TNode->left);
		cout << TNode->num << " ";
		PrintTree(TNode->right);
	}
}


void task1_func(TNode* TNode)
{
	if (TNode != NULL)
	{
		task1_func(TNode->left);
		if (TNode->num > 0)
			TNode->num = 0;

		if (TNode->num < 0)
			TNode->num = abs(TNode->num);

		task1_func(TNode->right);
	}
}
void task1()
{
	TNode* tree = NULL;

	for (int i = -5; i < 5; i++)
		tree = newTNode(i, tree);

	PrintTree(tree);
	task1_func(tree);
	cout << "\n";
	PrintTree(tree);
	cout << "\n";
}

void task2()
{
	vector <string> strVector;
	vector <string> strVector1;
	string str;
	vector <string> stack;
	char ch;
	int i = 0;
	ofstream file("file.txt");
	file << "Съешь же ещё этих вкусных французских булок да выпей чаю";
	file.close();

	ifstream file1("file.txt");
	while ((ch = file1.get()) != EOF) 
	{
		if (char(ch) != ' ') str = str + char(ch);
		else 
		{
			strVector.push_back(str);
			str.clear();
		}
	}    strVector.push_back(str);
	str.clear();
	file1.close();
	

	ofstream file2("file1.txt");
	file2 << "0 1 2 3 4 5 6 7 8 9";
	file2.close();

	ifstream file3("file1.txt");
	while ((ch = file3.get()) != EOF) {
		if (char(ch) != ' ') str = str + char(ch);
		else {
			strVector1.push_back(str);
			str.clear();
		}
	}    strVector1.push_back(str);
	str.clear();
	file3.close();

	for (int i = 0; i < 10; i++)
	{
		stack.push_back(strVector[i]);
		stack.push_back(strVector1[i]);
	}

	ofstream file4("file2.txt");
	for (int i = stack.size()-1; i >= 0; i--)
	{
		file4 << stack[i] <<" ";
		stack.pop_back();
	}

	cout << "Выполнено";
}
void task3()
{
	List <string> strList;
	int wordCount;
	string word;
	char firstSymbol;
	char firstSymbol_1 = 0;
	int count = 0;
	cout << "\n\nВведите количество слов, которых вы хотите внести в список: ";
	cin >> wordCount;
	cout << "\n\n";
	for (int i = 0; i < wordCount; i++)
	{
		cout << "Введите слово №" << i + 1 <<": ";
		cin >> word;

		if (i % 2 == 0)
		{
			firstSymbol = word[0];
			if (firstSymbol == firstSymbol_1)
				count++;
		}
		if (i % 2 == 1)
		{
			firstSymbol_1 = word[0];

			if (firstSymbol == firstSymbol_1)
				count++;
		}
		strList.push_back(word);
	}
	cout << "\nСписок:" << endl;
	for (int i = 0; i < strList.getListSize(); i++) {
		cout << strList[i] << endl;
	}

	cout <<"\nКоличество слов, первая буква которых совпадает с первой буквой следующего слова: " <<count;
}

void task4()
{
	int givenElement;
	int newElement;
	int number_of_entries;
	int count = 0;
	int x = 10;
	List<int> numList;
	for (int i = 0; i < 10; i++)
	{
		numList.push_back(rand() % 10);
	}

	cout << "\nСписок:" << endl;
	for (int i = 0; i < numList.getListSize(); i++) {
		cout << numList[i] << endl;
	}

	cout << "Количество вхождений в список: ";
	cin >> number_of_entries;

	for (int i = 0; i < number_of_entries; i++)
	{
		cout << "\nВхождение №" << i + 1 << endl;

		cout << "Заданный элемент: ";
		cin >> givenElement;
		
		cout << "Новый элемент: ";
		cin >> newElement;

		for (int i = 0; i < x; i++)
		{
			if (givenElement == numList[i])
			{
				count++;
				numList.insert(newElement, i);
				i++;
				x++;
			}
		}

		if (count == 0)
			cout << "\nЗаданный элемент в списке отсутствует\n";
		count = 0;

		cout << "\nСписок:" << endl;
		for (int i = 0; i < numList.getListSize(); i++) {
			cout << numList[i] << endl;
		}
	}
}

void task5()
{
	List<int> M;
	int E;
	int entry;
	int count = 0;

	for (int i = 0; i < 10; i++)
	{
		M.push_back(rand() % 10);
	}

	cout << "\nСписок:" << endl;
	for (int i = 0; i < M.getListSize(); i++) {
		cout << M[i] << endl;
	}
	for (int i = 0; i < 5; i++)
	{
		cout << "\nВведите элемент E: ";
		cin >> E;

		cout << "Входящий элемент: ";
		cin >> entry;

		if (E == entry)
		{
			M.push_front(entry);
			int deleteElement = rand() % 10;

			while (deleteElement == E)
				deleteElement = rand() % 10;

			for (int i = 0; i < 10; i++)
			{
				if (deleteElement == count)
				{
					M.erase(i);
					count = 0;
					break;
				}
				count++;
			}
		}

		else M.push_front(entry);
		cout << "\nСписок:" << endl;
		for (int i = 0; i < M.getListSize(); i++) {
			cout << M[i] << endl;
		}
	}
}
struct tNode 
{
	int num;
	tNode *left, *right;
};

int main()
{
	srand(time(0));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	setlocale(LC_ALL, "Russian");
	int choice = 0;
	
	while (choice != 6)
	{
		cout << "\n\n1. Деревья\n"
			<< "2. Стек\n"
			<< "3. Список\n"
			<< "4. Упорядочивание списка\n"
			<< "5. Удаление элементов из списка\n"
			<< "6. Выход\n"
			<< "Выберите действие: ";
		cin >> choice;
		cout << "\n";

		switch (choice)
		{
		case 1:
			{
				task1();
				break;
			}

		case 2:
			{
				task2();
				break;
			}

		case 3:
			{
				task3();
				break;
			}

		case 4:
			{
				task4();
				break;
			}

		case 5:
			{
				task5();
				break;
			}

		case 6: break;
		}
	}
	return 0;
}