﻿//#include <iostream>									//Задание 1
//#include <ctime>
//using namespace std;
//
//void Invert(int A[], const int N)
//{
//	cout << "Изначальный массив: \t";
//	for (int i = 0; i < N; i++)
//		cout << A[i] << "  ";
//
//	cout << "\nИнвертированный массив: ";
//	for (int i = 0; i < N / 2; i++)
//		swap(A[i], A[N - 1 - i]);
//	
//	for (int i = 0; i < N; i++)
//		cout << A[i] << "  ";
//}
//
//int main()
//{
//	setlocale(LC_ALL, "ru");
//	srand(time(0));
//	const int NA = 5;
//	const int NB = 10;
//	const int NC = 15;
//	int A[NA];
//	int B[NB];
//	int C[NC];
//
//	for (int i = 0; i < NA; i++)
//	{
//		A[i] = rand() % 100;
//	}
//
//	for (int i = 0; i < NB; i++)
//	{
//		B[i] = rand() % 100;
//	}
//
//	for (int i = 0; i < NC; i++)
//	{
//		C[i] = rand() % 100;
//	}
//
//	Invert(A, NA);
//	cout << "\n\n\n";
//	Invert(B, NB);
//	cout << "\n\n\n";
//	Invert(C, NC);
//	cout << "\n\n\n";
//
//	return 0;
//}


//#include <iostream>									//Задание 2
//#include <string>
//using namespace std;
//
//void LowCaseRus(string S)
//{
//	cout << "\nNew str: ";
//	for (int i = 0; i < S.length(); i++)
//	{
//		if (isupper(S[i]))
//			S[i] = tolower(S[i]);
//
//		cout << S[i];
//	}
//}
//int main()
//{
//	string S, S1, S2, S3, S4;
//	getline(cin, S);
//	getline(cin, S1);
//	getline(cin, S2);
//	getline(cin, S3);
//	getline(cin, S4);
//
//	cout << "\n\nDefault str: " << S;
//	LowCaseRus(S);
//	cout << "\n\n\Default str: " << S1;
//	LowCaseRus(S1);
//	cout << "\n\nDefault str: " << S2;
//	LowCaseRus(S2);
//	cout << "\n\nDefault str: " << S3;
//	LowCaseRus(S3);
//	cout << "\n\nDefault str: " << S4;
//	LowCaseRus(S4);
//
//	return 0;
//}



#include <iostream>										//Задание 3
using namespace std;

int Fib1(int N)												
{
		if (N == 0)
			return 0;

		if (N == 1)
			return 1;

		return Fib1(N - 1) + Fib1(N - 2);
}

int main()
{
	setlocale(LC_ALL, "ru");

	int N = 0;
	cout << "Элемент последовательности: ";
	cin >> N;

	for (int i = 0; i < 5; i++)
	{
		cout << Fib1(N-1) << "  ";
		N++;
	}

	return 0;
}